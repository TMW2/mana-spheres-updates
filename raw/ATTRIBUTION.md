For DMCA complaints please email dmca@tmw2.org and we will try to correct it
within 30 (thirty) days.

This special license file covers assets only, for code, you must refer to their
headers and to the COPYING file.

Notes: Works submitted by Jesusalva and under CC0, may also be used as if they
were under the Apache license without the need of prior consent.

----

GPLv2: Licensed under GNU General Public License version2 or later.
https://www.gnu.org/licenses/gpl-2.0.html
See also the GPL file.

GPLv3: Licensed under GNU General Public License version3 or later.
https://www.gnu.org/licenses/gpl-3.0.html
See also the COPYING

CC BY-SA 4.0: Licensed under Creative Commons Attribution-ShareAlike 4.0 Unported.
https://creativecommons.org/licenses/by-sa/4.0/
See also the CC BY-SA 4.0 file.

CC0 : Licensed under Creative Commons Zero
https://creativecommons.org/licenses/cc0
Also known as “Public Domain Dedication”

MIT : Licensed under the MIT License.
https://opensource.org/licenses/MIT
See also MIT file.

CC-BY : Either 3.0 or 4.0, depending if version was specified or not.
https://creativecommons.org/licenses/by/3.0/
https://creativecommons.org/licenses/by/4.0/

---------------------------------------------------------------------------------

Units at squared share the same license as their originals but contains edits.
Mobs with prefix 96 share the same license as their counterpart with prefix 95 but
are upscaled.

---------------------------------------------------------------------------------

This copyright notice must be kept intact for use, according to CC BY-SA
compatibility terms.
Where relevant, you must also include a link to https://tmw2.org in your credit.

DO NOT assume endorsement from the authors listed below!
The artworks may have been modified without notation about the edits in order to
work with this software, we strongly advise to look at their original sources
first! Some modifications may also be applied on-the-fly by the client.

Support the artists! Good Open Source art is hard to find.

---------------------------------------------------------------------------------
Useful links:
https://opengameart.org/	(Open Game Art / OGA)
https://arcmage.org/	(Arcmage - A libre CCG)
https://github.com/doofus-01	(Doofus 1/BfW Git repository)
https://www.peppercarrot.com/	(Pepper Carrot, webcomics)
https://davidrevoy.com/	(David Revoy personal blog)
https://www.indiedb.com/company/iron-thunder	(Iron Thunder, game dev)
https://serpentsoundstudios.com/	(Alexander Nakarada, music website)
https://wesnoth.org/	(The Battle for Wesnoth / BfW)

---------------------------------------------------------------------------------
# Folder
	path/to/the/licensed/work.something (Author)	(License)	(Notes/Contributors/Authors/Co-Authors/Sources/etc.)

# BG
	castle (yd)	(CC0)	(OpenGameArt)
	Human City (Santiago Iborra)	(CC BY SA)	(Arcmage)
	wilderness (Uncle Mungen)    (CC BY) (Lemma Soft Forums)

# MOBS
	.:: THE EMPIRE ::.
	950000 Spearman (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950001 Foot Soldier (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950002 Squad Leader (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950003 Conscript (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950004 Mounted Eagle (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950005 Band of Brothers (Santiago Iborra)	(CC BY SA)	(Arcmage)
	******

	.:: THE BANDITS ::.
	9501XX
	******

	.:: THE ELVES ::.
	950200 Archer (Kathrin Polikeit)	(CC BY SA)	(Arcmage)
	******

	.:: THE DARK HORDE ::.
	950300 Cyclops (Santiago Iborra)	(CC BY SA)	(Arcmage)
	******

	.:: DARKLANDERS ::.
	950400 Deadly Shock (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950401 Dark Mana Breather (Santiago Iborra)	(CC BY SA)	(Arcmage)
	******

	.:: THE REBELS ::.
	950500 Female Rebel 2 (Doofus-01)	(CC BY SA)	(Archaic Resources - BfW)
	******

	.:: THE LAND OF FIRE ::.
	950600 Drone (Doofus-01)	(CC BY SA)	(Archaic Era - BfW)
	950601 Mech Scout (Doofus-01)	(CC BY SA)	(Archaic Era - BfW)
	******

	.:: THE WIZARD ORDER ::.
	950700 Fire Wizard Female (Santiago Iborra)	(CC BY SA)	(Arcmage)
	950701 Delfador (Emilien Rotival)	(CC BY SA)	(Wesnoth - HttT)
	950702 Dimensional Pocket (Santiago Iborra)	(CC BY SA)	(Arcmage)
	******

	.:: THE DWARVES ::.
	950800 Dwarf Warrior (Santiago Iborra)	(CC BY SA)	(Arcmage)
	******

	.:: UNUSED OR GENERIC ::.
	950900 Brightshield (Doofus-01)	(CC BY SA)	(Archaic Era - BfW)
	950901 Jezzabell (Ironthunder)	(CC BY 4.0)	(OpenGameArt)
	950902 Cloak 2 (Doofus-01)	(CC BY SA)	(Archaic Era - BfW)
	
# DIALOG
	Foot Soldier (Santiago Iborra)	(CC BY SA)	(Arcmage)
	Squad Leader (Santiago Iborra)	(CC BY SA)	(Arcmage)
	Cloaked Man (Doofus-01)	(CC BY SA)	(Archaic Era - BfW)

# UNITS
	101001 Red Dragon Economist (David Revoy)	(CC BY)	(DavidRevoy.com)
	101002 Victor (David Revoy)	(CC BY)	(DavidRevoy.com)
	101003 Finding Balance (David Revoy)	(CC BY)	(DavidRevoy.com)
	101004 Speed Painting Test Kritas (David Revoy)	(CC BY)	(DavidRevoy.com)

	102001 Krita Speed Character Concept (David Revoy)	(CC BY)	(DavidRevoy.com)
	102002 Coriander, Concept Art (David Revoy)	(CC BY)	(DavidRevoy.com)
	102003 Krita Color Demo Videos (David Revoy)	(CC BY)	(DavidRevoy.com)
	102004 Gooseberry Dragon Sheep Design (David Revoy)	(CC BY)	(DavidRevoy.com)
	102005 Electron Donor (David Revoy)	(CC BY)	(DavidRevoy.com)

	103001 Flight of Spring (David Revoy)	(CC BY)	(DavidRevoy.com)
	103002 Witch of Magmah (David Revoy)	(CC BY)	(DavidRevoy.com)
	103003 03 Character Design (David Revoy)	(CC BY)	(DavidRevoy.com)
	103004 Christmas Card 2010 (David Revoy)	(CC BY)	(DavidRevoy.com/Edited)

	104001 Volcano Monster (David Revoy)	(CC BY SA)	(DavidRevoy.com)
	104002 Dawn Final Hi (David Revoy)	(CC BY SA)	(DavidRevoy.com)
	104003 Timelapse Lezard Revoy (David Revoy)	(CC BY SA)	(DavidRevoy.com)
	104004 Winter Fairies (David Revoy)	(CC BY)	(DavidRevoy.com)
	104005 Episode 33 WIP Grayscale (David Revoy)	(CC BY SA)	(DavidRevoy.com)

	105001 Nourished Flower (Santiago Iborra)	(CC BY SA)	(Arcmage)
	105002 Powerful (David Revoy)	(CC BY)	(DavidRevoy.com)
	105003 Owl Princess (David Revoy)	(CC BY SA)	(DavidRevoy.com)
	105004 HEROART (Mohammed Agbadi/Sine Nomine Publishing)	(CC0)	(OGA/Edited)

	900001 Fairy on RPG dice 20 (David Revoy)	(CC BY)	(peppercarrot.com/Edited)
	900002 Fairy on RPG dice 20 (David Revoy)	(CC BY)	(peppercarrot.com/Edited)
	900003 Fairy on RPG dice 20 (David Revoy)	(CC BY)	(peppercarrot.com/Edited)
	900004 Fairy on RPG dice 20 (David Revoy)	(CC BY)	(peppercarrot.com/Edited)
	900005 Fairy on RPG dice 20 (David Revoy)	(CC BY)	(peppercarrot.com/Edited)

	900021 Treasure (Ironthunder)	(CCBY)	(bg by kindland[CC0]/Edited)

	900011 Mana Orb (Jesusalva)	(CC0)	(Eggs by OnTheBus, bg by kindland/Edited)
	900012 Mana Orb (Jesusalva)	(CC0)	(Eggs by OnTheBus, bg by kindland/Edited)
	900013 Mana Orb (Jesusalva)	(CC0)	(Eggs by OnTheBus, bg by kindland/Edited)
	900014 Mana Orb (Jesusalva)	(CC0)	(Eggs by OnTheBus, bg by kindland/Edited)
	900015 Mana Orb (Jesusalva)	(CC0)	(Eggs by OnTheBus, bg by kindland/Edited)

	900031 Mana Orb (Jesusalva)	(CC0)	(Eggs by OnTheBus, bg by kindland/Edited)

# BANNER
	xmas2020 (Jesusalva)	(CC BY)	(Contains artwork from David Revoy)

